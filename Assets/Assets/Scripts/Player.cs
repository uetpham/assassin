﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D _rigidPlayer;
    [SerializeField]
    private float _jumpForce = 1.0f;
    [SerializeField]
    private bool _grounded = false;
    // Start is called before the first frame update
    void Start()
    {
        //get các thành phần trong Rigidbody2D
        _rigidPlayer = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        JumpPlayer();
    }
    public void MovPlayer()
    {
        float _movPlayer = Input.GetAxisRaw("Horizontal");
        //velocity đại lượng thể hiện tốc độ di chuyển của vật thể
        _rigidPlayer.velocity = new Vector2(_movPlayer, _rigidPlayer.velocity.y);
    }
    public void JumpPlayer()
    {
        //RaycastHit2D hitInfo =
        if (Input.GetKeyDown(KeyCode.Space) && _grounded)
        {
            _rigidPlayer.velocity = new Vector2(_rigidPlayer.velocity.x, _jumpForce);
            Debug.Log("_rigidPlayer.velocity" + _rigidPlayer.velocity);
        }
    }
}
